﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
//using System.Diagnostics;

namespace dz08_reflection
{
    public class MyClass
    {
        public int i1, i2, i3, i4, i5;
        public static MyClass Get() => new MyClass() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }

    public static class SerializatorExt
    {
        public static string ToCsv(this object o) => string.Join("\n", o.GetType().GetFields().Select(x => x.Name + " = " + x.GetValue(o).ToString()));
        
        public static object FromCsv(object obj,string st) {
            foreach (string s in st.Split('\n'))
            {
                string x = s.ToString();
                int posi = x.IndexOf("=");
                string FieldName = x.Substring(0, posi).Trim();
                string FieldValue = x.Substring(posi+1, x.Length- posi-1).Trim();
                FieldInfo fInfo = obj.GetType().GetField(FieldName);


                if(fInfo.FieldType.Name=="Int32" )
                    fInfo.SetValue(obj, Convert.ToInt32(FieldValue));
                else fInfo.SetValue(obj, FieldValue);              
            }
            return obj;
        }
    }


    class Program
    {
        static void GetTimeSerialization()
        {
            var v = MyClass.Get();
            var s = "";
            for (int i = 0; i< 100_000; i++)
            {
                s = SerializatorExt.ToCsv(v);
                //s = Newtonsoft.Json.JsonConvert.SerializeObject(v);
                //s = $"i1={v.i1}\ni2={v.i2}\ni3={v.i3}\ni1={v.i4}\ni1={v.i4}\ni5={v.i5}";
            }           
            Console.WriteLine(s);
        }
        static void GetTimeDeserialization()
        {
            //string s = "i1 = 10\ni2 = 20\ni3 = 30\ni4 = 40\ni5 = 50";
            string s = "{'i1' : 10,'i2' : 20,'i3' : 30,'i4' : 40,'i5' : 50}";
            var v = MyClass.Get();
            for (int i = 0; i < 1_000_000; i++)
            {
                v = (MyClass)SerializatorExt.FromCsv(v, s);
                //v = Newtonsoft.Json.JsonConvert.DeserializeObject<MyClass>(s);
            }
            Console.WriteLine(SerializatorExt.ToCsv(v));
        }

        static void Main(string[] args)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            GetTimeSerialization();
            stop.Stop();
            Console.WriteLine("Serialization duration=" + stop.ElapsedMilliseconds.ToString());

            stop.Start();
            GetTimeDeserialization();
            stop.Stop();
            Console.WriteLine("Deserialization duration=" + stop.ElapsedMilliseconds.ToString());


            
            


            Console.ReadKey();


        }
    }
}
